package genki

import (
	"encoding/json"
	"net/http"
	"strings"
	"sync"
)

type Health map[string]Pinger

func (h Health) Handler(logger interface{ Print(v ...any) }) http.HandlerFunc {
	type P struct {
		string
		bool
	}

	return func(w http.ResponseWriter, r *http.Request) {
		wg, pings, index := new(sync.WaitGroup), make([]P, len(h)), 0
		for name, pinger := range h {
			wg.Add(1)
			go func(i int, n string, p Pinger) {
				defer wg.Done()
				pings[i] = P{n, p.Ping(r.Context())}
			}(index, name, pinger)

			index++
		}
		wg.Wait()

		checks, status, statusCode := make(map[string]bool, len(h)), true, http.StatusOK
		for _, ping := range pings {
			if name := ping.string; ping.bool {
				checks[name] = true
			} else {
				checks[name], status, statusCode = false, false, http.StatusInternalServerError
			}
		}

		if !status {
			names := []string{}
			for pinger, ping := range checks {
				if !ping {
					names = append(names, pinger)
				}
			}

			logger.Print("no reply from [" + strings.Join(names, " ") + "]")
		}

		w.Header().Set("content-type", "application/json")
		w.WriteHeader(statusCode)

		err := json.NewEncoder(w).Encode(map[string]any{
			"status": status,
			"checks": checks,
		})
		if err != nil {
			logger.Print(err)
		}
	}
}
