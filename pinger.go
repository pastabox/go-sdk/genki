package genki

import "context"

type Pinger interface{ Ping(context.Context) bool }

type PingerFunc func(context.Context) bool

func (p PingerFunc) Ping(ctx context.Context) bool {
	return p(ctx)
}
