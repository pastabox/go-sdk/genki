package genki

import (
	"net/http"
	"time"
)

var DefaultReadyDate = time.Now().Add(3 * time.Second)

func Ready() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if time.Now().Before(DefaultReadyDate) {
			w.WriteHeader(http.StatusInternalServerError)
		}
	}
}

func ReadyAfter(date time.Time) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if time.Now().Before(date) {
			w.WriteHeader(http.StatusInternalServerError)
		}
	}
}

func ReadyIn(dur time.Duration) http.HandlerFunc {
	return ReadyAfter(time.Now().Add(dur))
}
